/*
  Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
*/

#include "HLTSeedingRoIToolDefs.h"

namespace HLTSeedingRoIToolDefs {
  namespace eFexEM {
    const char ContainerName[] = "L1_eEMRoI";
    const char ThresholdType[] = "eEM";
  }
  namespace eFexTau {
    const char ContainerName[] = "L1_eTauRoI";
    const char ThresholdType[] = "eTAU";
  }
  namespace jFexTau {
    const char ContainerName[] = "L1_jFexTauRoI";
    const char ThresholdType[] = "jTAU";
  }
  namespace Muon {
    const char ContainerName[] = "LVL1MuonRoIs";
    const char ThresholdType[] = "MU";
  }
}
