Modules in this directory
-----

* [TrigElectronFactories](TrigElectronFactories.py)
  * ToolFactories to configure egammaAlgs to be used at the HLT
* [ElectronRecoSequences](ElectronRecoSequences.py)
  * Assembles the sequences for fast reconstruction
* [PrecisionElectronRecoSequences](PrecisionElectronRecoSequences.py)
  * Assembles the sequences for precision reconstruction
* [PrecisionElectronRecoSequences_GSF](PrecisionElectronRecoSequences_GSF.py)
  * Assembles the sequences for precision reconstruction with Gaussian Sum Filter tracks
* [TrigEMBremCollectionBuilder](TrigEMBremCollectionBuilder.py)
  * ToolFactory to instantiate TrigEMBremCollectionBuilder with default configuration
* [generateElectron](generateElectron.py)
  * Prototype for NewJO chain configuration